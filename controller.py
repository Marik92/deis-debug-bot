# -*- coding-utf8 -*-

import re
import datetime
import telepot,time,os
import zbxtg_settings
import subprocess
from subprocess import Popen, PIPE
from pprint import pprint
from pyzabbix import ZabbixAPI
from datetime import timedelta


# Telegram senders id
authorized_senders = zbxtg_settings.tg_authorized_senders

now = datetime.datetime.now()
for_message = []

def start_message(msg):
    '''This function send start message text with help info'''

    chat_id = msg['chat']['id']
    start_text = '''#### Welcome to DEIS_Debug_Bot!!! ####
    This bot can use some debug tools for network troubleshooting.
    For example:
    1) /ping x.x.x.x                  # ICMP Request to IP-address
    2) /mtr x.x.x.x                   # MTR to IP-address
    3) /nmap x.x.x.x                  # Check open ports of HOST
    4) /whois x.x.x.x                 # Resolve DNS
    5) /curl x.x.x.x                  # Check HTTP/HTTPS answer
    6) /check                         # Start DEIS_Check_Bot
    7) /find_zbx_host "HOSTNAME"      # Find zbx-host and ID with name 
    8) /find_zbx_graph "HOST-ID"      # Find zbx-graphs of host
    9) /get_zbx_graph "GRAPH-ID"      # Get zbx-graph image
    '''

    bot.sendMessage(chat_id, start_text)


def ping(address_dict):
        """
        This function ping all addresses in dict 
        and return list with result
        """

        for name,ip in address_dict.items():
                result = subprocess.Popen(['ping', '-c', '2', ip], stdout=PIPE).wait()
                if result == 0:
                        for_message.append('✅' + str(name) + ' - ' + str(ip))
                else:
                        for_message.append('❌' + str(name) + ' - ' + str(ip))
        return (for_message)


def handle(msg):
    zapi = ZabbixAPI(zbxtg_settings.zbx_server)
    zapi.login(zbxtg_settings.zbx_api_user, zbxtg_settings.zbx_api_pass)
    print("Connected to Zabbix API Version %s" % zapi.api_version())

    chat_id = msg['chat']['id']
    text = msg['text']
    sender = msg['from']['id']
    with open('zbx-api.log', 'a') as LOG_FILE:
        LOG_FILE.write("Chat-id - " + str(chat_id) + " Text - " + str(text) + " Sender - " + str(sender) + "\n")

    if sender in authorized_senders:
      
      args = text.split()

      command = args[0]

      if command == '/start':
            start_message(msg)

      if command == '/help':
            start_message(msg)

      if command == '/ping':
            host = str(args[1])
            output = os.popen("ping -c 3 " + host).read()
            bot.sendMessage(chat_id, output)

      if command == '/mtr':
            help_text = 'Please wait a couple of minute...'
            bot.sendMessage(chat_id, help_text)
            host = str(args[1])
            output = os.popen("mtr --report " + host).read()
            bot.sendMessage(chat_id, output)

      if command == '/nmap':
            help_text = 'Please wait a couple of minute...'
            bot.sendMessage(chat_id, help_text)
            value = str(args[1])
            host = str(args[2])
            output = os.popen("nmap -A " + value + " " + host).read()
            bot.sendMessage(chat_id, output)

      if command == '/curl':
            help_text = 'Please wait a couple of minute...'
            bot.sendMessage(chat_id, help_text)
            host = str(args[1])
            output = os.popen("curl -Iv " + host).read()
            bot.sendMessage(chat_id, output)

      if command == '/whois':
            help_text = 'Please wait a couple of minute...'
            bot.sendMessage(chat_id, help_text)
            host = str(args[1])
            output = os.popen("whois " + host).read()
            bot.sendMessage(chat_id, output)

      if command == '/check':
            help_text = 'Please wait a couple of minute...'
            bot.sendMessage(chat_id, help_text)
            output = '\n'.join(ping(zbxtg_settings.address))
            bot.sendMessage(chat_id, output)

      if command == '/find_zbx_host':
            if not args[1:]:
                bot.sendMessage(chat_id, 'Please enter hostname or part of hostname')
            else:
                help_text = 'Please wait a couple of minute...'
                bot.sendMessage(chat_id, help_text)
                host_match = str(' '.join(args[1:]))
                find_hosts = []
                for h in zapi.host.get(output="extend"):
                    if host_match in h['host']:
                        find_hosts.append(h['host'] + '(Host ID: ' + h['hostid'] + ')')
                result = '\n'.join(find_hosts)
                if len(result) > 4096:
                    for x in range (0, len(result), 4096):
                        bot.sendMessage(chat_id, result[x:x+4096])
                else:
                     bot.sendMessage(chat_id, result)

      if command == '/find_zbx_graph':
            if not args[1]:
                bot.sendMessage(chat_id, 'Please enter Host-ID')
            elif str(args[1]).isdigit() == False:
                bot.sendMessage(chat_id, 'Host-ID consist only of numbers')
            else:
                help_text = 'Please wait a couple of minute...'
                bot.sendMessage(chat_id, help_text)
                host = args[1]
                find_graphs = []
                for h in zapi.graph.get(hostids=host, output="extend"):
                    value = h['name'] + '(Graph ID: ' + h['graphid'] + ')'
                    find_graphs.append(value)
                result = '\n'.join(find_graphs)
                if len(result) > 4096:
                    for x in range (0, len(result), 4096):
                        bot.sendMessage(chat_id, result[x:x+4096])
                else:
                     bot.sendMessage(chat_id, result)

      if command == '/get_zbx_graph':
            if not args[1]:
                bot.sendMessage(chat_id, 'Please enter Graph-ID')
            elif not args[2]:
                bot.sendMessage(chat_id, 'Please enter Day Period')
            elif str(args[1]).isdigit() == False:
                bot.sendMessage(chat_id, 'Graph_ID consist only of numbers')
            elif str(args[2]).isdigit() == False:
                bot.sendMessage(chat_id, 'Day Period consist only of numbers')
            elif int(args[2]) == 0:
                bot.sendMessage(chat_id, 'Day Period must be grater than 0')
            elif int(args[2]) > 365:
                bot.sendMessage(chat_id, 'Day Period must be less than 365')
            else:
                bot.sendMessage(chat_id, 'Please wait...')
                graphid = args[1]
                days = args[2]
                delta = timedelta(days=int(days))
                time_range = now - delta
                time_range = str(time_range).split(' ')
                output = os.popen("python3 zbx-api.py " + str(chat_id) + " " + str(graphid) + " " + str(time_range[0])).read()


bot = telepot.Bot(zbxtg_settings.tg_key)
bot.message_loop(handle)


while 1:
    time.sleep(10)
